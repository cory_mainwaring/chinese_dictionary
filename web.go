package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"bitbucket.org/cory_mainwaring/pinyin"
)

func ServeSearch(w http.ResponseWriter, r *http.Request, d *Dictionary) {
	SearchTerm, UnescapeError := url.QueryUnescape(r.URL.String())
	if UnescapeError != nil {
		fmt.Fprintf(w, "Couldn't understand query: %s", UnescapeError)
	}
	Output, _ := json.Marshal(d.Search(SearchTerm))
	fmt.Fprintf(w, "%s", Output)
}

func ServeUpdate(w http.ResponseWriter, r *http.Request, d *Dictionary) {
	r.ParseForm()
	form := r.Form
	var e = Entry{
		Simplified:  form.Get("Simplified"),
		Traditional: form.Get("Traditional"),
		Pinyin:      pinyin.New(form.Get("Pinyin")),
		Definitions: strings.Split(form.Get("Definition"), " / "),
	}
	Updated := false
	for i, v := range d.Entries {
		if v.Simplified == e.Simplified {
			d.Entries[i] = e
			Updated = true
		}
	}

	if !Updated {
		Index := len(d.Entries)
		d.Entries = append(d.Entries, e)
		d.SimplifiedIndex[e.Simplified] = Index
		d.TraditionalIndex[e.Traditional] = Index
	}
	fmt.Println(d.Save())
}

func ServeDefault(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./static/formatter.html")
}

type DictionaryHandleFunc func(http.ResponseWriter, *http.Request, *Dictionary)

func DictionaryHandler(d *Dictionary, function DictionaryHandleFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		function(w, r, d)
	})
}

func StartServer(d *Dictionary) {
	http.Handle("/search/", http.StripPrefix("/search/", DictionaryHandler(d, ServeSearch)))
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	http.HandleFunc("/update", DictionaryHandler(d, ServeUpdate))
	http.HandleFunc("/", ServeDefault)
	http.ListenAndServe(":8000", nil)
}
