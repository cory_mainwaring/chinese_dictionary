package main

import (
	"bitbucket.org/cory_mainwaring/pinyin"
	"strings"
)

type Entry struct {
	Simplified  string
	Traditional string
	Pinyin      pinyin.Pinyin
	Definitions []string
}

func NewEntry(Simplified, Traditional, Pinyin string, Definitions []string) Entry {
	e := Entry{
		Simplified:  Simplified,
		Traditional: Traditional,
		Pinyin:      pinyin.New(Pinyin),
		Definitions: Definitions,
	}

	return e
}

func (e *Entry) Contains(Searchterm string) bool {
	if strings.Contains(e.Simplified, Searchterm) ||
		strings.Contains(e.Traditional, Searchterm) ||
		e.Pinyin.Contains(Searchterm) ||
		strings.Contains(strings.Join(e.Definitions, " / "), Searchterm) {
		return true
	}
	return false
}

func (e *Entry) Match(Searchterm string) bool {
	if e.Simplified == Searchterm ||
		e.Traditional == Searchterm ||
		strings.Join(e.Definitions, " / ") == Searchterm {
		return true
	} else if pinyin.Possible(Searchterm) && e.Pinyin.Match(Searchterm) {
		return true
	}
	return false
}
