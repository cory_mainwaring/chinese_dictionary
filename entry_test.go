package main

import (
	"testing"

	"bitbucket.org/cory_mainwaring/pinyin"
	. "github.com/smartystreets/goconvey/convey"
)

var simplifiedPart1 = "经"
var simplifiedPart2 = "济"
var testSimplified = simplifiedPart1 + simplifiedPart2

var traditionalPart1 = "經"
var traditionalPart2 = "濟"
var testTraditional = traditionalPart1 + traditionalPart2

var pinyinPart1 = "jing1"
var pinyinPart2 = "ji4"
var testPinyin = pinyinPart1 + pinyinPart2
var testDefinitions = []string{"economy", "economic"}
var testEntry = Entry{
	Simplified:  testSimplified,
	Traditional: testTraditional,
	Pinyin:      pinyin.New(testPinyin),
	Definitions: testDefinitions,
}

func TestNewEntry(t *testing.T) {

	Convey("Given various components", t, func() {
		resultEntry := NewEntry(testSimplified, testTraditional, testPinyin, testDefinitions)
		Convey("Output the right Entry", func() {
			So(resultEntry.Simplified, ShouldEqual, testEntry.Simplified)
			So(resultEntry.Traditional, ShouldEqual, testEntry.Traditional)
			So(resultEntry.Pinyin, ShouldEqual, testEntry.Pinyin)
			So(resultEntry.Definitions, ShouldResemble, testEntry.Definitions)
		})

	})

}

func TestContains(t *testing.T) {
	Convey("Contains Simplified", t, func() {
		So(testEntry.Contains(testSimplified), ShouldBeTrue)
		So(testEntry.Contains(simplifiedPart1), ShouldBeTrue)
		So(testEntry.Contains(simplifiedPart2), ShouldBeTrue)
	})
	Convey("Contains Traditional", t, func() {
		So(testEntry.Contains(testTraditional), ShouldBeTrue)
		So(testEntry.Contains(traditionalPart1), ShouldBeTrue)
		So(testEntry.Contains(traditionalPart2), ShouldBeTrue)
	})
	Convey("Contains Pinyin", t, func() {
		So(testEntry.Contains(testPinyin), ShouldBeTrue)
		So(testEntry.Contains(pinyinPart1), ShouldBeTrue)
		So(testEntry.Contains(pinyinPart2), ShouldBeTrue)
	})
	Convey("Contains Definitions", t, func() {
		So(testEntry.Contains(testDefinitions[0]), ShouldBeTrue)
		So(testEntry.Contains(testDefinitions[1]), ShouldBeTrue)
	})
	Convey("Doesn't contain cheese", t, func() {
		So(testEntry.Contains("cheese"), ShouldBeFalse)
	})
}

func TestMatch(t *testing.T) {
	Convey("Matches Simplified", t, func() {
		So(testEntry.Match(testSimplified), ShouldBeTrue)
	})
	Convey("Matches Traditional", t, func() {
		So(testEntry.Match(testTraditional), ShouldBeTrue)
	})
	Convey("Matches Pinyin", t, func() {
		So(testEntry.Match(testPinyin), ShouldBeTrue)
	})
	Convey("Matches Definition", t, func() {
		So(testEntry.Match(testDefinitions[0]+" / "+testDefinitions[1]), ShouldBeTrue)
	})
	Convey("Doesn't match a variety of things", t, func() {
		So(testEntry.Match(testDefinitions[0]), ShouldBeFalse)
		So(testEntry.Match("cheese"), ShouldBeFalse)
	})
}
