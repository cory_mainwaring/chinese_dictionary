'use strict';
// TODO(cory): add support for multiple phrases, change render method to show phrases,
// have output be another thing entirely, with it's own textbox type thing
// TODO(cory): Toggle button for Quizlet Output
// TODO(cory): auto-add output to a file

var SeperatorCount = 37;
var GlobalSeperator = '~';
var GlobalSeperatorReplacement = '-';

var Output = document.getElementById('output');

function BurnChildren(el) {
    while(el.hasChildNodes()) {
        el.removeChild(el.firstChild);
    }
}

function UpdateDictionary(form) {
    $.post("/update", $(form).serialize(), function(data) {
        CurrentPhrase().Render();
    }, 'text');
}

function StripHTML(s) {
    var stripper = document.createElement("div");
    stripper.innerHTML = s;
    return stripper.textContent;
}

Node.prototype.hasParent = function(node) {
    if (this.parentElement === document || this.parentElement === null) {
        return false;
    }
    if (this.parentElement === node) {
        return this.parentElement;
    }
    return this.parentElement.hasParent(node);
};

var Phrase = function(Options) {
    var Options = Options || {};
    var English = Options.English || "";
    var Chinese = Options.Chinese || "";
    this.Words = Options.Words || [];
    this.DOM = this.BuildDOM();
    Object.defineProperties(this, {
        "Chinese": {
            enumerable:true,
            get: function() {
                return Chinese;
            },
            set: function(Value) {
                Chinese = Value;
                this.Render();
            }
        },
        "English": {
            enumerable:true,
            get: function() {
                return English;
            },
            set: function(Value) {
                English = Value;
                this.Render();
            }
        }
    });

    var P = this;

    return this;
};

Phrase.prototype.BuildDOM = function() {
    var self = this;

    var Container = document.createElement("div");

    var InputContainer = document.createElement("div");
    InputContainer.className = "container";

    var ChineseInput = document.createElement("div");
    ChineseInput.className = "editarea";
    ChineseInput.id = "chinese";
    ChineseInput.contentEditable = true;

    ChineseInput.selectionHandler = function(sel) {
        var range = sel.getRangeAt(0);
        if (range.endContainer !== range.startContainer) {
            AddDialog.Hide();
            return;
        }
        var range_rect = range.getBoundingClientRect();
        AddDialog.Show(range_rect.left, range_rect.bottom, range);
    }

    ChineseInput.onkeyup = ChineseInput.onmouseup = function() {
        var sel = window.getSelection();
        if (sel.type === "Range") {
            if (sel.anchorNode.hasParent(ChineseInput) !== false) {
                ChineseInput.selectionHandler(sel);
            }
        } else {
            AddDialog.Hide();
        }
        self.Chinese = ChineseInput.innerHTML;
        phraseList.Render(false);
    }


    var EnglishInput = document.createElement("div");
    EnglishInput.className = "editarea";
    EnglishInput.id = "english";
    EnglishInput.contentEditable = true;

    EnglishInput.onkeyup = EnglishInput.onmouseup = function() {
        self.English = EnglishInput.innerHTML;
        phraseList.Render(false);
    }

    InputContainer.appendChild(ChineseInput);
    InputContainer.appendChild(EnglishInput);

    var ButtonContainer = document.createElement("div");
    ButtonContainer.className = "container";
    
    var AutoAddButton = document.createElement("button");
    AutoAddButton.innerText = "Auto-Add Words";
    AutoAddButton.onclick = function() {
        var ChineseInput = self.ChineseInput;
        var Re = /\{([^\{]+)\}/g;
        var WordArray = [];
        while ((WordArray = Re.exec(ChineseInput.textContent)) !== null) {
            ChineseInput.innerHTML = ChineseInput.innerHTML.replace(new RegExp(WordArray[0]), "<i>"+WordArray[1]+"</i>");
            CurrentPhrase().AddWord(new Word({Text:WordArray[1], Phrase:CurrentPhrase()}));
            Re.lastIndex -= 2;
        }
        self.Chinese = ChineseInput.innerHTML;
        self.English = EnglishInput.innerHTML;
    }
    ButtonContainer.appendChild(AutoAddButton);

    var PhraseWords = document.createElement("div");
    PhraseWords.id = "phrase-words";

    Container.appendChild(InputContainer);
    Container.appendChild(ButtonContainer);
    Container.appendChild(PhraseWords);
    console.log(Container);

    return Container;

};

Phrase.prototype.AddWord = function(WordToAdd) {
    this.Words.push(WordToAdd);
    this.SortWords();
    this.FindDefinitions();
    this.Render();
}

Phrase.prototype.FindDefinitions = function() {
    var P = this;
    for (var Index in this.Words) {
        var XHR = new XMLHttpRequest();
        var CurrentWord = this.Words[Index];
        (function(w, Index) {
            XHR.addEventListener("load", function(event) {
                try {
                    var Defs = JSON.parse(event.target.responseText);
                    if (w.Text !== null) {
                        for (var DictIndex in Defs) {
                            var TestWord = Defs[DictIndex];
                            if (TestWord.Simplified == w.Text || TestWord.Traditional == w.Text) {
                                P.Words[Index].Text = null;
                                P.Words[Index].Simp = TestWord.Simplified;
                                P.Words[Index].Trad = TestWord.Traditional;
                                P.Words[Index].Pinyin = TestWord.Pinyin;
                                P.Words[Index].Def = TestWord.Definitions.join(" / ");
                            }
                        }
                        P.Render();
                    }
                } catch (e) {
                    // Do nothing
                }
            });
        })(CurrentWord, Index);
        
        // TODO(cory): make work on other fields
        var SearchTerm = this.Words[Index].Text;
        XHR.open("GET", "/search/"+SearchTerm);
        XHR.send();
    }
}

Phrase.prototype.SortWords = function() {
    var P = this;
    this.Words = this.Words.sort(function(a, b) {
        var indexA = 0;
        if (a.Simp || a.Trad) {
            indexA = P.Chinese.indexOf(a.Simp);
            if (indexA < 0) {
                indexA = P.Chinese.indexOf(a.Trad);
            }
        } else {
            indexA = P.Chinese.indexOf(a.Text);
        }
        var indexB = 0;
        if (b.Simp || b.Trad) {
            indexB = P.Chinese.indexOf(b.Simp);
            if (indexB < 0) {
                indexB = P.Chinese.indexOf(b.Trad);
            }
        } else {
            indexB = P.Chinese.indexOf(b.Text);
        }
        if (indexA < indexB) {
            return -1;
        } else if (indexA == indexB) {
            return 0;
        } else {
            return 1;
        }
    });
}

Phrase.prototype.RemoveEmphasis = function(w) {

    var ChineseInput = this.DOM.querySelector("#chinese");

    var Re = new RegExp("<i>("+w.Simp+"|"+w.Trad+"|"+w.Text+")</i>", "g");
    ChineseInput.innerHTML = ChineseInput.innerHTML.replace(Re, "$1");
    this.Chinese = ChineseInput.innerHTML;

}

Phrase.prototype.Render = function() {
    var Target = this.DOM.querySelector("#phrase-words");
    BurnChildren(Target);
    var ListElement = document.createElement("ul");
    for (var Index in this.Words) {
        var DefElement = document.createElement("li");
        var CurrentWord = this.Words[Index];
        DefElement.appendChild(CurrentWord.DOM);
        CurrentWord.Render();
        ListElement.appendChild(DefElement);
    }
    Target.appendChild(ListElement);
}

Phrase.prototype.QuizletOutput = function() {
// Format: Simplified (Traditional)\tDefinition\nModified_Sentence~ 
    var Result = "";
    var QuizletifySentence = function(phrase, word) {
        phrase = phrase.replace(/<i>([^<]*)<\/i>/g, function(a,b,c,d){return b;});
        
        return phrase.replace(new RegExp(word.Simp +"|" + word.Trad, "g"), Array(word.Simp.length+1).join('~'));
    };
    for (var i in this.Words) {
        var w = this.Words[i];
        Result += w.Simp + "(" + w.Trad + ")\n[" + w.Pinyin + "]\t" + w.Def + "\n" + QuizletifySentence(this.Chinese, w) + "@";
    }
    return Result;
};

Phrase.prototype.AnkiOutput = function() {
    var C = this.Chinese || "";
    var E = this.English || "";
    var Result = C.replace(GlobalSeperator, GlobalSeperatorReplacement) + GlobalSeperator;
    Result += E.replace(GlobalSeperator, GlobalSeperatorReplacement) + GlobalSeperator;

    var words = this.Words || [];
    for (var i=0; i < words.length;i++) {
        var currentWord = words[i];
        Result += currentWord.Print();
    }
    var NumSeps = Result.match(/~/g).length;
    Result += GlobalSeperator.repeat(SeperatorCount-NumSeps);
    return Result;

};

var Word = function(Options) {
    this.Text = Options.Text || null;
    this.Simp = Options.Simp || null;
    this.Trad = Options.Trad || null;
    this.Pinyin = Options.Pinyin || null;
    this.Def = Options.Def || null;
    this.DOM = this.BuildDOM();
    this.Phrase = Options.Phrase || null;
    this.Editing = false;
    return this;
};

Word.prototype.BuildDOM = function() {
        var w = this;

        var WordContainer = document.createElement("div");
        WordContainer.className = "word-container";
        WordContainer.draggable = true;

        WordContainer.ondblclick = function(e) { 
            w.Edit();
        };

        var WordElement = document.createElement("div");
        WordElement.className = "word-element";

        WordContainer.appendChild(WordElement);

        var DelButton = document.createElement("button");
        DelButton.className = "delete-button";
        DelButton.innerText = "X";

        DelButton.onclick = function() {
            w.Phrase.RemoveEmphasis(w)
            for (var Index in w.Phrase.Words) {
                if (w.Phrase.Words[Index].Equals(w)) {
                    w.Phrase.Words.splice(Index, 1);
                    w.Phrase.SortWords();
                }
            }
            w.Phrase.Render();
        }

        WordContainer.insertBefore(DelButton, WordContainer.firstChild);

        return WordContainer;
}

Word.prototype.FieldIsNotNull = function(FieldName) {
    if (this[FieldName] === null) {
        return false;
    }
    return true;
}

Word.prototype.FieldMatches = function(Comparison, FieldName) {
    if (this[FieldName] === Comparison[FieldName]) {
        return true;
    }
    return false;
}
Word.prototype.FieldMatchChecked = function(Comparison, FieldName) {
    if (Comparison.FieldIsNotNull(FieldName) &&
            this.FieldIsNotNull(FieldName) &&
            this.FieldMatches(Comparison, FieldName)) {
        return true;
    }
    return false;
}

Word.prototype.Equals = function(Comparison) {
    if (this.FieldMatchChecked(Comparison, "Simp") ||
        this.FieldMatchChecked(Comparison, "Trad") ||
        this.FieldMatchChecked(Comparison, "Text") ) {
        return true;
    }
    return false;
}

Word.prototype.Edit = function(container) {
    var self = this;

    self.Editing = true;
    self.Render(container);
}

Word.prototype.Print = function() {
    var Result = "";

    if (this.Simp) {
        Result += this.Simp.replace(GlobalSeperator, GlobalSeperatorReplacement);
    }
    Result += GlobalSeperator;

    if (this.Trad) {
        Result += this.Trad.replace(GlobalSeperator, GlobalSeperatorReplacement);
    }
    Result += GlobalSeperator;

    if (this.Pinyin) {
        Result += this.Pinyin.replace(GlobalSeperator, GlobalSeperatorReplacement);
    }
    Result += GlobalSeperator;

    if (this.Def) {
        Result += this.Def.replace(GlobalSeperator, GlobalSeperatorReplacement);
    }
    Result += GlobalSeperator;
    return Result;
}

Word.prototype.Render = function() {
    var self = this;

    var Map = {
        "Traditional": self.Trad,
        "Simplified": self.Simp,
        "Pinyin": self.Pinyin,
        "Definition": self.Def
    };

    if (self.Text !== null) {
        Map.Text = self.Text;
    }

    var Container = this.DOM;
    var Element = Container.querySelector(".word-element");
    
    BurnChildren(Element);

    var Line = function(name, value) {
        var line = document.createElement("p");

        var label = document.createElement("label");
        label.className = "word-field-label";
        label.appendChild(document.createTextNode(name + ": "));
        line.appendChild(label);

        if (self.Editing) {
            var input = document.createElement("input");
            input.type = "text";
            input.name = name;
            input.id = name;
            input.value = value;
            label.setAttribute("for", name);
            line.appendChild(input);
        } else {
            var Span = document.createElement("span");
            Span.className = "word-field-value";
            Span.appendChild(document.createTextNode(value));
            line.appendChild(Span);
        }

        return line;
    };

    for (var Field in Map) {
        Element.appendChild(Line(Field, Map[Field]));
    }

    if (self.Editing) {
        Container.classList.add("word-container-edit");
        Container.draggable = false;

        var UpdateButton = document.createElement("button");
        UpdateButton.innerText = "Save Globally";
        Element.appendChild(UpdateButton);
        UpdateButton.onclick = function(event) {
            self.Simp = Element.querySelector("input[name=Simplified]").value;
            self.Trad = Element.querySelector("input[name=Traditional]").value;
            self.Pinyin = Element.querySelector("input[name=Pinyin]").value;
            self.Def = Element.querySelector("input[name=Definition]").value;
            if (self.Simp !== null || self.Trad !== null) {
                self.Text = null;
            }
            self.Editing = false;
            UpdateDictionary($(Element).find("input"));
            self.Render();
        };

        var SaveButton = document.createElement("button");
        SaveButton.innerText = "Save Locally";
        Element.appendChild(SaveButton);
        (function(Button, Target, Word){
            Button.onclick = function(event) {
                Word.Simp = Target.querySelector("input[name=Simplified]").value;
                Word.Trad = Target.querySelector("input[name=Traditional]").value;
                Word.Pinyin = Target.querySelector("input[name=Pinyin]").value;
                Word.Def = Target.querySelector("input[name=Definition]").value;
                if (Word.Simp !== null || Word.Trad !== null) {
                    Word.Text = null;
                }
                Word.Editing = false;
                Word.Render(Target);
            };
        })(SaveButton, Element, self);

        var CancelButton = document.createElement("button");
        CancelButton.innerText = "Cancel";
        Element.appendChild(CancelButton);
        CancelButton.onclick = function(event) {
            self.Editing = false;
            self.Render(Element);
        };
    } else {
        Element.parentElement.classList.remove("word-container-edit");
        Element.parentElement.draggable = true;
    }
}

var CurrentPhrase = function() {
    return phraseList.Phrases[phraseList.CurrentIndex];
};

var AddDialog = document.getElementById('add-dialog');
AddDialog.AddButton = AddDialog.querySelector('#add');
AddDialog.Done = function(Range) {
    var I = document.createElement('i');
    CurrentPhrase().AddWord(new Word({Text:Range.toString(), Phrase:CurrentPhrase()}));
    Range.surroundContents(I);
    this.Hide();
    return I;
}
//TODO(cory): Fix bug typing after putting in <i> tag keeps everything in <i> tag
AddDialog.Show = function(X, Y, Range) {
    this.setAttribute("style", "left:"+X+";top:"+Y+";");
    this.hidden = false;
    this.AddButton.onclick = function(e) {
        AddDialog.Done(Range);
    }
}
AddDialog.Hide = function() {
    this.hidden = true;
}
AddDialog.Hide();


document.getElementById("anki-output-button").onclick = function() {
    var Result = "";
    for (var Index in phraseList.Phrases) {
        Result += phraseList.Phrases[Index].AnkiOutput() + "\n";
    }
    Output.value = Result;
}

document.getElementById("quizlet-output-button").onclick = function() {
    var Result = "";
    for (var Index in phraseList.Phrases) {
        Result += phraseList.Phrases[Index].QuizletOutput();
    }
    Output.value = Result;
}

var PhraseList = function(options) {
    this.Phrases = options.Phrases || [new Phrase()];
    this.CurrentIndex = 0;
    this.DOM = this.BuildDOM();
    return this;
};

PhraseList.prototype.BuildDOM = function() {
    var Container = document.createElement("div");

    var ListContainer = document.createElement("div");
    ListContainer.className = "container";

    var List = document.createElement("ul");
    List.id = "phrase-list";

    ListContainer.appendChild(List);

    var PhraseContainer = document.createElement("div");
    PhraseContainer.id = "phrase";

    Container.appendChild(ListContainer);
    Container.appendChild(PhraseContainer);

    return Container;

}

PhraseList.prototype.Render = function(fullRender) {
    var self = this;
    var Target = this.DOM.querySelector("#phrase-list");
    BurnChildren(Target);

    for (var Index in this.Phrases) {
        var phrase = this.Phrases[Index];
        var li = document.createElement("li");
        if (Index == this.CurrentIndex) {
            li.classList.add("current");
        }
        (function(i) {
            li.onclick = function(event) {
                self.CurrentIndex = i;
                self.Render(true);
                CurrentPhrase().Render();
            }
        })(Index)
        var text = "...";
        var english = StripHTML(phrase.English);
        var chinese = StripHTML(phrase.Chinese);
        if (english !== "") {
            text = english;
        } else if (chinese !== "") {
            text = chinese;
        }
        li.innerHTML = text;
        Target.appendChild(li);
    }

    var addPhraseLi = document.createElement("button");
    addPhraseLi.classList.add("add-phrase");
    addPhraseLi.innerText = "+";
    addPhraseLi.onclick = function(event) {
        self.CurrentIndex = self.Phrases.length;
        self.Phrases.push(new Phrase());
        self.Render(true);
        CurrentPhrase().Render();
    };
    Target.appendChild(addPhraseLi);
    if (fullRender) {
        var c = CurrentPhrase();
        BurnChildren(this.DOM.querySelector("#phrase"));
        this.DOM.querySelector("#phrase").appendChild(c.DOM);
    }
};

var phraseList = new PhraseList({
    Phrases: [new Phrase()]
});
phraseList.Render(true);
document.querySelector("#phrase-list-container").appendChild(phraseList.DOM);
CurrentPhrase().Render();


