package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"runtime/pprof"
	"strings"
	"time"
)

func main() {
	var RunServer bool
	var DictionaryAdditionFilename string
	var CpuProfile string
	flag.BoolVar(&RunServer, "s", false, "Determines whether to run a server on port 8000 to search for words")
	flag.StringVar(&DictionaryAdditionFilename, "a", "", "File from which to add entries to the dictionary")
	flag.StringVar(&CpuProfile, "profile", "", "Write Profile to file")
	flag.Parse()

	if CpuProfile != "" {
		f, err := os.Create(CpuProfile)
		if err != nil {
			fmt.Println(err)
			return
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	DictionaryFilename := flag.Arg(0)
	if DictionaryFilename == "" {
		fmt.Println("Need a file to pull the dictionary from")
		return
	}

	MainDictionary, Error := NewDictionary(DictionaryFilename)
	if Error != nil {
		fmt.Println(Error)
		return
	}

	fmt.Println("Dictionary Length:", len(MainDictionary.Entries))

	if DictionaryAdditionFilename != "" {
		AdditionFile, err := os.Open(DictionaryAdditionFilename)
		if err != nil {
			fmt.Println("Couldn't open addition file: ", err)
		} else {
			MainDictionary.AddFromFile(AdditionFile)
			err = MainDictionary.Save()
			if err != nil {
				fmt.Println("Couldn't save Dictionary: ", err)
			}
			AdditionFile.Close()
		}
	}

	if RunServer {
		go StartServer(MainDictionary)
	}

	var InputReader = bufio.NewReader(os.Stdin)
	var InputString string
	for {
		fmt.Printf("Enter command: ")
		InputString, _ = InputReader.ReadString('\n')
		InputString = strings.Trim(InputString, "\n\r ")
		if strings.HasPrefix(InputString, "search ") {
			SearchString := strings.Split(InputString, "search ")[1]
			StartTime := time.Now()
			Output, _ := json.Marshal(MainDictionary.Search(SearchString))
			os.Stdout.Write(Output)
			fmt.Println("")
			fmt.Println(time.Now().Sub(StartTime))
		} else if strings.HasPrefix(InputString, "quit") {
			MainDictionary.Save()
			break
		} else {
			fmt.Printf("Bad Command\n")
		}
	}

}
