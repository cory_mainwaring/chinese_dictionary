package main

import (
	"bufio"
	"encoding/gob"
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"
)

type Dictionary struct {
	Entries          []Entry
	Filename         string
	SimplifiedIndex  map[string]int
	TraditionalIndex map[string]int
}

func NewDictionary(Filename string) (*Dictionary, error) {
	d := Dictionary{}
	d.Filename = Filename

	// NOTE(cory): Check if Dictionary File already exists
	var DictionaryFileSize int64 = 0
	DictionaryFileInfo, FileInfoError := os.Stat(d.Filename)
	if FileInfoError == os.ErrNotExist {
		DictionaryFileSize = 0
	} else if FileInfoError != nil {
		return nil, FileInfoError
	} else {
		DictionaryFileSize = DictionaryFileInfo.Size()
	}

	if DictionaryFileSize == 0 {
		DictionaryFile, FileCreateError := os.Create(d.Filename)
		if FileCreateError != nil {
			return nil, FileCreateError
		}
		defer DictionaryFile.Close()

		Encoder := gob.NewEncoder(DictionaryFile)
		EncodingError := Encoder.Encode(d.Entries)
		if EncodingError != nil {
			return nil, EncodingError
		}
	} else {
		// NOTE(cory): Load Dictionary From File
		DictionaryFile, FileOpenError := os.OpenFile(d.Filename, os.O_RDWR, 0666)
		if FileOpenError != nil {
			return nil, FileOpenError
		}
		defer DictionaryFile.Close()

		Decoder := gob.NewDecoder(DictionaryFile)
		DecodingError := Decoder.Decode(&d.Entries)
		if DecodingError != nil {
			return nil, DecodingError
		}
	}

	d.Index()

	return &d, nil
}

// TODO: make case insensitive for latin characters
func (d *Dictionary) Search(Searchterm string) []Entry {
	Result := make([]Entry, 0, 10)
	Terms := strings.Fields(Searchterm)
	for _, SubTerm := range Terms {
		if Entry, Ok := d.SimplifiedIndex[SubTerm]; Ok {
			Result = append(Result, d.Entries[Entry])
		} else if Entry, Ok := d.SimplifiedIndex[SubTerm]; Ok {
			Result = append(Result, d.Entries[Entry])
		}
	}
	if len(Result) > 0 {
		return Result
	}
	for _, SearchEntry := range d.Entries {
		viable := true
		for _, SubTerm := range Terms {
			if !SearchEntry.Match(SubTerm) {
				viable = false
				break
			}
		}
		if viable {
			Result = append(Result, SearchEntry)
		}
	}

	if len(Result) < 1 {
		return nil
	}

	return Result
}

func (d *Dictionary) IndexOf(Searchterm string) int {
	if Index, Ok := d.SimplifiedIndex[Searchterm]; Ok {
		return Index
	}

	if Index, Ok := d.TraditionalIndex[Searchterm]; Ok {
		return Index
	}

	return -1
}

// TODO(cory): Isolate from fmt
func (d *Dictionary) AddFromFile(AdditionFile *os.File) error {
	DictionaryScanner := bufio.NewScanner(AdditionFile)
	Index := 0
	EntryRegex := regexp.MustCompile(`([^\s]+)\s+([^\s]+)\s+\[([^\]]+)\]\s+([^\n]+)`)
	var StartTime = time.Now()

	fmt.Printf("Building Dictionary")
	for DictionaryScanner.Scan() {
		Index++
		if Index%10000 == 0 {
			fmt.Printf(".")
		}
		Text := DictionaryScanner.Text()

		if strings.HasPrefix(Text, "#") {
			continue
		}

		RegexMatch := EntryRegex.FindStringSubmatch(Text)

		if RegexMatch == nil {
			fmt.Printf("\nEntry on line %d is formatted incorrectly.\nMoving on", Index)
			continue
		}

		var entry = NewEntry(
			RegexMatch[2],
			RegexMatch[1],
			RegexMatch[3],
			strings.Split(strings.Trim(RegexMatch[4], "/"), "/"),
		)

		entryIndex := d.IndexOf(entry.Simplified)
		if entryIndex < 0 {
			entryIndex = d.IndexOf(entry.Traditional)
		}

		if entryIndex >= 0 {
			d.Entries[entryIndex] = entry
		} else {
			d.Entries = append(d.Entries, entry)
		}

	}
	fmt.Printf("\nFinished! Spent %1.2f seconds.\n", time.Now().Sub(StartTime).Seconds())

	if err := DictionaryScanner.Err(); err != nil {
		return err
	}

	return nil
}

func (d *Dictionary) Save() error {
	fmt.Println("Saving Dictionary...")

	DictionaryFile, FileOpenError := os.OpenFile(d.Filename, os.O_RDWR, 0666)
	if FileOpenError != nil {
		return FileOpenError
	}
	defer DictionaryFile.Close()

	DictionaryFile.Seek(0, 0)
	DictionaryFile.Truncate(0)

	DictionaryEncoder := gob.NewEncoder(DictionaryFile)

	EncodingError := DictionaryEncoder.Encode(d.Entries)
	if EncodingError != nil {
		return EncodingError
	}

	return nil
}

func (d *Dictionary) Index() {
	d.SimplifiedIndex = make(map[string]int, len(d.Entries))
	d.TraditionalIndex = make(map[string]int, len(d.Entries))
	for Index, Value := range d.Entries {
		d.SimplifiedIndex[Value.Simplified] = Index
		d.TraditionalIndex[Value.Traditional] = Index
	}
}
